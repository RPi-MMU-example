#ifndef RPI_MMU_EXAMPLE_INTERRUPTS_H
#define RPI_MMU_EXAMPLE_INTERRUPTS_H

#include <stdint.h>

// ARM control block
// called "base address for the ARM interrupt register" elsewhere
#define ARM_BASE (PERIF_BASE + 0xB000)
#define ARM_IRQ_BASIC_PENDING  (ARM_BASE + 0x200)
#define ARM_IRQ_PENDING_1      (ARM_BASE + 0x204)
#define ARM_IRQ_PENDING_2      (ARM_BASE + 0x208)
#define ARM_FIQ_CONTROL        (ARM_BASE + 0x20C)
#define ARM_ENABLE_IRQS_1      (ARM_BASE + 0x210)
#define ARM_ENABLE_IRQS_2      (ARM_BASE + 0x214)
#define ARM_ENABLE_BASIC_IRQS  (ARM_BASE + 0x218)
#define ARM_DISABLE_IRQS_1     (ARM_BASE + 0x21C)
#define ARM_DISABLE_IRQS_2     (ARM_BASE + 0x220)
#define ARM_DISABLE_BASIC_IRQS (ARM_BASE + 0x224)
//offset of peripherals+ offset for first addresable register for interupt controller
#define RPI_INTERRUPT_CONTROLLER_BASE   ( 0x3F000000UL + 0xB200 )
// Bits in the Enable_Basic_IRQs register to enable various interrupts.
// According to the BCM2835 ARM Peripherals manual, section 7.5 */
#define RPI_BASIC_ARM_TIMER_IRQ         (1 << 0)
#define RPI_BASIC_ARM_MAILBOX_IRQ       (1 << 1)
#define RPI_BASIC_ARM_DOORBELL_0_IRQ    (1 << 2)
#define RPI_BASIC_ARM_DOORBELL_1_IRQ    (1 << 3)
#define RPI_BASIC_GPU_0_HALTED_IRQ      (1 << 4)
#define RPI_BASIC_GPU_1_HALTED_IRQ      (1 << 5)
#define RPI_BASIC_ACCESS_ERROR_1_IRQ    (1 << 6)
#define RPI_BASIC_ACCESS_ERROR_0_IRQ    (1 << 7)

// The interrupt controller memory mapped register set
typedef struct {
    volatile uint32_t IRQ_basic_pending;
    volatile uint32_t IRQ_pending_1;
    volatile uint32_t IRQ_pending_2;
    volatile uint32_t FIQ_control;
    volatile uint32_t Enable_IRQs_1;
    volatile uint32_t Enable_IRQs_2;
    volatile uint32_t Enable_Basic_IRQs;
    volatile uint32_t Disable_IRQs_1;
    volatile uint32_t Disable_IRQs_2;
    volatile uint32_t Disable_Basic_IRQs;
} rpi_irq_controller_t;

extern rpi_irq_controller_t* RPI_GetIrqController(void);
#endif //RPI_MMU_EXAMPLE_INTERRUPTS_H
