// driver for the read-only ramfs
// see makefs.c for details

#include <stdint.h>
#include "ramfs.h"
#include "strings.h"

static inline char *align4(char *addr)
{
  return (char*) (((uint32_t) addr - 1) & ~0b11) + 4;
}

int find_file(void *ramfs, char *filename, struct ramfile *buf)
{
  char *fs_file = ramfs;

  while (*fs_file)
    {
      uint32_t *fs_file_size = (uint32_t*)
	align4(fs_file + strlen(fs_file) + 1);
      
      char *fs_file_contents = (char*) (fs_file_size + 1);

      if (!strcmp(fs_file, filename))
	{
	  buf->file_size = *fs_file_size;

	  buf->file_name = fs_file;

	  buf->file_contents = fs_file_contents;
	  
	  return 0;
	}

      // move to the next file in ramfs
      fs_file = align4(fs_file_contents + *fs_file_size);
    }

  return -1; // reached end of ramfs; file not found
}
