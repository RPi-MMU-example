#ifndef MEMORY_H
#define MEMORY_H

#include <stddef.h>

// These macros were heavily used b4 I moved all the address
// computation to the linker script. Now I'm just keeping them
// in case they're needed for something else :)
#define POWER_OF_2(EXP) (((size_t) 1) << EXP)

#define ALIGN_POWER_OF_2(ADDR, EXP)				\
  (((ADDR - 1) & ~(POWER_OF_2(EXP) - 1)) + POWER_OF_2(EXP))

#define SECTION_SIZE POWER_OF_2(20)

#define ALIGN_SECTION(ADDR) ALIGN_POWER_OF_2(ADDR, 20)


// memory layout

#define INTERRUPT_VECTOR_TABLE_START ((uint32_t) 0x0)

// all those symbols are defined in the linker script
extern char __end;
extern char __start;
extern char _translation_table_start;
extern char _translation_table_end;
extern char _sections_list_start;
extern char _sections_list_end;
extern char _stack_start;
extern char _fiq_stack_start;
extern char _fiq_stack_top;
extern char _irq_stack_start;
extern char _irq_stack_top;
extern char _supervisor_stack_start;
extern char _supervisor_stack_top;
extern char _stack_end;

#define KERNEL_START ((size_t) &__start) // this is 0x0
#define KERNEL_END   ((size_t) &__end)

// first 2^14 aligned address after the kernel
#define TRANSLATION_TABLE_BASE ((size_t) &_translation_table_start)
#define TRANSLATION_TABLE_END  ((size_t) &_translation_table_end)

// another 32KB after the translation table are used for sections list
#define SECTIONS_LIST_START ((size_t) &_sections_list_start)
#define SECTIONS_LIST_END  ((size_t) &_sections_list_end)

// first section after the translation table is left unused;
// the next section is used as the stack
#define STACK_START            ((size_t) &_stack_start)
#define FIQ_STACK_START        ((size_t) &_fiq_stack_start)
#define FIQ_STACK_END          ((size_t) &_fiq_stack_top)
#define IRQ_STACK_START        ((size_t) &_irq_stack_start)
#define IRQ_STACK_END          ((size_t) &_irq_stack_top)
#define SUPERVISOR_STACK_START ((size_t) &_supervisor_stack_start)
#define SUPERVISOR_STACK_END   ((size_t) &_supervisor_stack_top)
#define STACK_END              ((size_t) &_stack_end)

#define PRIVILEGED_MEMORY_END STACK_END


// the following describes the virtual section for our PL0 programs
#define PL0_SECTION_NUMBER ((size_t) 0xaaa)

#define VIRTUAL_PL0_MEMORY_START (PL0_SECTION_NUMBER << 20)
#define VIRTUAL_PL0_MEMORY_END			\
  (VIRTUAL_PL0_MEMORY_START + SECTION_SIZE)

#endif // MEMORY_H

