#ifndef RAMFS_H
#define RAMFS_H

struct ramfile
{
  char *file_name;
  uint32_t file_size;
  char *file_contents;
};

// search for file named filename in ramfs;
// If found - return 0 and fill buf fields with file's info.
// Otherwise return a non-zero value.
int find_file(void *ramfs, char *filename, struct ramfile *buf);

#endif // RAMFS_H
