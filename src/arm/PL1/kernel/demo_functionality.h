#ifndef DEMO_FUNCTIONALITY_H
#define DEMO_FUNCTIONALITY_H

void demo_paging_support(void);

void demo_current_mode(void);

//void demo_setup_libkernel(void);

void demo_setup_PL0(void);

//void demo_go_unprivileged(void);

//void demo_setup_interrupts(void);

#endif // DEMO_FUNCTIONALITY_H
