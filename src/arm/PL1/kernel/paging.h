#ifndef PAGING_H
#define PAGING_H

void setup_flat_map(void);

void setup_pager_structures(uint32_t available_mem);

#define SECTION_NULL 0xffff

// returns section number or SECTION_NULL in case of failure
uint16_t claim_and_map_section
(void *owner, uint16_t where_to_map, uint8_t access_permissions);

#endif // PAGING_H
