#ifndef BCMCLOCK_H
#define BCMCLOCK_H

#include <stdint.h>
#include "interrupts.h"
#include "global.h"

#define ST_BASE (PERIF_BASE + 0x3000) // System Timer

#define ST_CS  (ST_BASE + 0x0)  // System Timer Control/Status
#define ST_CLO (ST_BASE + 0x4)  // System Timer Counter Lower 32 bits
#define ST_CHI (ST_BASE + 0x8)  // System Timer Counter Higher 32 bits
#define ST_C0  (ST_BASE + 0xC)  // System Timer Compare 0
#define ST_C1  (ST_BASE + 0x10) // System Timer Compare 1
#define ST_C2  (ST_BASE + 0x14) // System Timer Compare 2
#define ST_C3  (ST_BASE + 0x18) // System Timer Compare 3

static inline void bcmclk_enable_timer_irq(void)
{
  wr32(ARM_ENABLE_IRQS_1, 1 << 3);
}

static inline void bcmclk_disable_timer_irq(void)
{
  wr32(ARM_DISABLE_IRQS_1, 1 << 3);
}

static inline void bcmclk_irq_settimeout(uint32_t timeout)
{
  uint32_t clock_now = rd32(ST_CLO);
  wr32(ST_C3, clock_now + timeout);
  wr32(ST_CS, 1 << 3);
}

#endif // BCMCLOCK_H
