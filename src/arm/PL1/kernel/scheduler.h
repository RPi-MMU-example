#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <stdint.h>

#include "psr.h"

extern PSR_t PL1_PSR;

void setup_scheduler_structures(void);

// to be called by irq handler when respective uart interrupt happens
void scheduler_try_output(void);

// to be called by irq handler when respective uart interrupt happens
void scheduler_try_input(void);

void __attribute__((noreturn))
schedule_wait_for_output(uint32_t regs[14], char c);

void __attribute__((noreturn))
schedule_wait_for_input(uint32_t regs[14]);

void __attribute__((noreturn))
schedule_save_context(uint32_t regs[14]);

void __attribute__((noreturn)) schedule(void);

void __attribute__((noreturn))
schedule_new(uint32_t pc, uint32_t sp);

#endif
