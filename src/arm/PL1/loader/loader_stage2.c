#include <stddef.h>
#include <stdint.h>
#include "uart.h"
#include "io.h"
#include "global.h"

void *const kernel_load_addr = ((void*) 0x8000);

void _stage2_main(uint32_t r0, uint32_t r1, uint32_t atags)
{
  uart_init();

  // get kernel size via uart (little endian)
  uint32_t b0, b1, b2, b3;
  
  b0 = getchar();
  b1 = getchar();
  b2 = getchar();
  b3 = getchar();
  
  uint32_t kernel_size = b0 | (b1 << 8) | (b2 << 16) | (b3 << 24);

  // load kernel at kernel_load_addr
  char *dst = kernel_load_addr, *end = dst + kernel_size;

  while (dst < end)
    *(dst++) = getchar();

  // jump to kernel
  ((void(*)(uint32_t, uint32_t, uint32_t)) kernel_load_addr)
    (r0, r1, atags);
}

