#ifndef GLOBAL_H
#define GLOBAL_H

#include <stdint.h>

// board type, raspi2
#define RASPI 2

// conditionally #define PERIF_BASE
#if RASPI == 4

#define PERIF_BASE 0xFE000000

#elif RASPI == 3 || RASPI == 2

#define PERIF_BASE 0x3F000000

#else // if RASPI == 1

#define PERIF_BASE 0x20000000

#endif

// GPIO_BASE is #define'd in terms of PERIF_BASE
// (as in sane kernels - like linux, not like in wiki.osdev codes...)
#define GPIO_BASE (PERIF_BASE + 0x200000)

inline static uint32_t rd32(uint32_t addr)
{
  return *(uint32_t volatile*) addr;
}

inline static void wr32(uint32_t addr, uint32_t value)
{
  *(uint32_t volatile*) addr = value;
}

#endif // GLOBAL_H
