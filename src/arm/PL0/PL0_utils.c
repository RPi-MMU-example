#include <stddef.h>
#include <stdint.h>

#include "svc_interface.h"
#include "PL0_utils.h"

// most generic definition possible
// the actual function defined in svc.S
uint32_t svc(enum svc_type, ...);

void putchar(char character)
{
  svc(UART_PUTCHAR, character);
}

char getchar(void)
{
  return svc(UART_GETCHAR);
}
