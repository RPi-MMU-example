#include "PL0_utils.h"

// entry point - must remain the only function in the file!
void PL0_main(void)
{
  // If loading program to userspace and handling of svc are
  // implemented correctly, this shall get printed
  puts("Hello userspace! Type 'f' if you want me to try accessing "
       "kernel memory!");

  while (1)
    {
      char c = getchar();
      
      if (c == '\r')
	putchar('\n');
      
      putchar(c);
      
      if (c == 'f')
	{
	  // if we're indeed in PL0, we should trigger the abort
	  // handler now, when trying to access memory we're not
	  // allowed to
	  puts("Attempting to read kernel memory from userspace :d");
	  char first_kernel_byte[2];

	  first_kernel_byte[0] = *(char*) 0x0;
	  first_kernel_byte[1] = '\0';

	  puts(first_kernel_byte);
	}
    }
}
