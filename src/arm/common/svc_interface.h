#ifndef SVC_INTERFACE_H
#define SVC_INTERFACE_H

enum svc_type
  {
    UART_PUTCHAR,
    UART_GETCHAR,
    UART_WRITE
  };

#endif // SVC_INTERFACE_H
