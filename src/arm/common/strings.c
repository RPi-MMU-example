#include "strings.h"

void uint32_to_dec(uint32_t number, char buf[10])
{
  for (int i = 0; i < 10; i++)
    {
      buf[10 - 1 - i] = '0' + (number % 10);
      number /= 10;
    }
}

void uint32_to_hex(uint32_t number, char buf[8])
{
  for (int i = 0; i < 8; i++)
    {
      unsigned char quadbit = (number >> ((8 - i - 1) * 4)) & 0xf;
      buf[i] =  quadbit > 9 ? quadbit - 10 + 'a' : quadbit + '0';
    }
}

void uint32_to_bin(uint32_t number, char buf[32])
{
  for (int i = 0; i < 32; i++)
    buf[i] = ((number >> (32 - i - 1)) & 1) ? '1' : '0';
}

void uint32_to_decstring(uint32_t number, char buf[11])
{
  uint32_to_dec(number, buf);
  buf[10] = '\0';
}

void uint32_to_hexstring(uint32_t number, char buf[9])
{
  uint32_to_hex(number, buf);
  buf[8] = '\0';
}

void uint32_to_binstring(uint32_t number, char buf[33])
{
  uint32_to_bin(number, buf);
  buf[32] = '\0';
}

void trim_0s(char string[])
{
  size_t i;
  for (i = 0; string[i] == '0'; i++);
  
  size_t j = 0;
  
  if (!string[i])
    string[j++] = string[--i];

  do
    string[j] = string[i + j];
  while (string[j++]);
}

void uint32_to_decstringt(uint32_t number, char buf[11])
{
  uint32_to_decstring(number, buf);
  trim_0s(buf);
}

void uint32_to_hexstringt(uint32_t number, char buf[9])
{
  uint32_to_hexstring(number, buf);
  trim_0s(buf);
}

void memcpy(void *dst, void *src, size_t nbytes)
{
  size_t iter;

  // copying by word is faster than by byte,
  // but can easily cause alignment faults, so we resign from it...
  for (iter = 0; iter < nbytes ; iter++)
    ((volatile uint8_t*) dst)[iter] = ((uint8_t*) src)[iter];
}

// keep in mind memset is also needed for array initialization, like
// uint32_t buf[16] = {0};
// gcc compiles this to memset call

void *memset(void *s, int c, size_t n)
{
  volatile char *mem = s;
  
  for (size_t i = 0; i < n; i++)
    mem[i] = c;
  
  return s;
}

char *strcat(char *dst, const char *src)
{
  char *where_to_append;

  for (where_to_append = dst; *where_to_append; where_to_append++);
  
  size_t i;
  
  for (i = 0; src[i]; i++)
    ((char volatile*) where_to_append)[i] = src[i];

  ((char volatile*) where_to_append)[i] = '\0';

  return dst;
}

int strcmp(char const *str1, char const *str2)
{
  while (1)
    {
      int c1 = (unsigned char) *str1, c2 = (unsigned char) *str2;
      
      if (!c1 && !c2)
	return 0;

      if (c1 != c2)
	return c1 - c2;

      str1++; str2++;
    }
}

size_t strlen(char const *str1)
{
  size_t len = 0;

  while (str1[len])
    len++;

  return len;
}

