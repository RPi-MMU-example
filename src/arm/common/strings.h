#ifndef STRINGS_H
#define STRINGS_H

#include <stddef.h>
#include <stdint.h>

void uint32_to_dec(uint32_t number, char buf[10]);

void uint32_to_hex(uint32_t number, char buf[8]);

void uint32_to_bin(uint32_t number, char buf[32]);

void uint32_to_decstring(uint32_t number, char buf[11]);

void uint32_to_hexstring(uint32_t number, char buf[9]);

void uint32_to_binstring(uint32_t number, char buf[33]);

void trim_0s(char string[]);

void uint32_to_decstringt(uint32_t number, char buf[11]);

void uint32_to_hexstringt(uint32_t number, char buf[9]);

void memcpy(void *dst, void *src, size_t nbytes);
  
void *memset(void *s, int c, size_t n);

char *strcat(char *dst, const char *src);
  
int strcmp(char const *str1, char const *str2);

size_t strlen(char const *str1);

#endif // STRINGS_H
