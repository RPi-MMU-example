#include <stddef.h>

#include "io.h"
#include "strings.h"

void puts(char string[])
{
  prints(string);

  putchar('\n');
  putchar('\r');
}

void prints(char string[])
{
  for (size_t i = 0; string[i]; i++)
    putchar(string[i]);
}

void error(char string[])
{
  prints("ERROR! ");
  puts(string);
  while (1);
}

void printdec(uint32_t number)
{
  char buf[11];

  uint32_to_decstring(number, buf);

  prints(buf);
}

void printhex(uint32_t number)
{
  char buf[9];

  uint32_to_hexstring(number, buf);

  prints(buf);
}

void printbin(uint32_t number)
{
  char buf[33];

  uint32_to_binstring(number, buf);

  prints(buf);
}

void printdect(uint32_t number)
{
  char buf[11];

  uint32_to_decstringt(number, buf);

  prints(buf);
}

void printhext(uint32_t number)
{
  char buf[9];

  uint32_to_hexstringt(number, buf);

  prints(buf);
}

