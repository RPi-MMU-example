#ifndef IO_H
#define IO_H

#include <stdint.h>

// putchar() and getchar() are not part of io.c, but it's useful to
// have those symbols declared here
void putchar(char c);

char getchar(void);

void puts(char string[]);

void prints(char string[]);

void error(char string[]);

void printdec(uint32_t number);

void printhex(uint32_t number);

void printbin(uint32_t number);

void printdect(uint32_t number);

void printhext(uint32_t number);

#endif // IO_H
